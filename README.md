
## Based on
Angular 7+, Angular CLI, TypeScript, Scss, Bootstrap

## Demo

[Live Demo](http://treesflower.com/ng-pi-admin)

## Getting started
```
git clone https://github.com/bknds/ng-pi-admin.git

npm install

ng serve 

localhost:4200
```

## License
[MIT license](LICENSE)
