import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MailService {
  EndPoint = environment.API_ENDPOINT + `/mails/sendMail`;

  constructor(private http: HttpClient) {}

  create(payload: any): Observable<any> {
    return this.http.post(this.EndPoint, payload);
  }
}
