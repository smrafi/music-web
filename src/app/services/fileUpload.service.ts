import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class FileUploadService {
  EndPoint = environment.API_ENDPOINT + `/fileuploads/`;

  constructor(private http: HttpClient) {}

  uploadImage(payload: any): Observable<any> {
    return this.http.post(this.EndPoint + 'uploadImage', payload);
  }

  uploadFile(payload: any): Observable<any> {
    return this.http.post(this.EndPoint + 'uploadFile', payload);
  }

  getFile(filename: any): Observable<any> {
    return this.http.get<Blob>(
      `${this.EndPoint}getfile?filePath=uploads/${filename}`, {
        responseType: 'blob' as 'json'
      }
    );
  }

  // getFactoryPdf(year: number, factory_id: number, project_id: number) {
  //   return this.http.get<Blob>(
  //     `${
  //       this.EndPoint
  //     }factory-report?year=${year}&factory_id=${factory_id}&project_id=${project_id}`,
  //     {
  //       responseType: 'blob' as 'json'
  //     }
  //   );
  // }
}
// http://localhost:3000/api/v1/fileuploads/getfile%3FfilePath=uploads/0c0567a299c13e0cc482e2e1afe59dae
