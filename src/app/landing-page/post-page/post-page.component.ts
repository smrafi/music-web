import { Router } from '@angular/router';
import { StorageService } from './../../services/storage.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.scss'],
  providers: [StorageService]
})
export class PostpageComponent implements OnInit {
  url: any;
  access_group: any;
  constructor(private storageService: StorageService, private router: Router) {}

  ngOnInit() {
    this.url = this.router.url;
    // console.log(this.router.url); //  /routename

    this.access_group = this.storageService.getCurrentAccessGroup();
    //console.log(this.access_group);
  }
}
