import { StorageService } from './../../../../services/storage.service';
import { GlobalService } from './../../../../services/global.service';
import { PostService } from './../../../../services/post.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss'],
  providers: [PostService, GlobalService, StorageService]
})
export class PostItemComponent implements OnInit {
  postData: any;
  justified = 'justified';
  userPost: any;
  id: any;
  likes: any;
  access_group: any;
  currentData: any;
  image = [
    './../../../../../assets/images/0zgS8TXVXx0.jpg',
    './../../../../../assets/images/7b_Kgqr956Y.jpg',
    './../../../../../assets/images/H5BLz_4lJqg.jpg',
    './../../../../../assets/images/c_5aUOJ-A8c.jpg',
    './../../../../../assets/images/pHwrlGEIxs8.jpg',
    './../../../../../assets/images/9uWblAxO308.jpg',
    './../../../../../assets/images/Dp91Gclv2_A.jpg',
    './../../../../../assets/images/HwhJ3xgf77Y.jpg'
  ];
  constructor(
    private postService: PostService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.access_group = this.storageService.getCurrentAccessGroup();
    this.id = this.storageService.getCurrentUserId();
    this.getData();
  }

  getData() {
    this.postService.getAll().subscribe((result: any) => {
      this.postData = result.data;

      // console.log('aa', this.postData);
    });
    this.postService.postByUser(this.id).subscribe((result: any) => {
      // console.log(this.id);
      this.userPost = result.data;
      // console.log(this.id, this.userPost);
    });
  }

  like(id) {
    this.postService.getById(id).subscribe((result: any) => {
      this.likes = result.data.likes;
      // this.likes = this.likes + 1;
      // console.log((this.likes = this.likes + 1));
    });
    // console.log(id);
    this.postService
      .update(id, {
        likes: this.likes + 1
      })
      .subscribe((result: any) => {
        this.getData();
      });
  }

  // openModal(modal, id) {
  //   this.postService.getById(id).subscribe((result: any) => {
  //     this.currentData = result.data;
  //     //console.log(this.currentData);
  //   });
  //   modal.open();
  // }
  // closeModal(modal) {
  //   modal.close();
  // }
}
