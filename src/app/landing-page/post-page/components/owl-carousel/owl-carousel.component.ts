import { PostService } from './../../../../services/post.service';
import { Component, OnInit } from '@angular/core';
import { SwiperConfig, SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-owl-carousel',
  templateUrl: './owl-carousel.component.html',
  styleUrls: ['./owl-carousel.component.scss'],
  providers: [PostService]
})
export class OwlCarouselComponent implements OnInit {
  imageSources = [
    './../../../../../assets/images/carousel/01-TALENT-014-1110x480.jpg',
    './../../../../../assets/images/carousel/11109174-1110x480.jpg',
    './../../../../../assets/images/carousel/CEphRDYWoAE7IH1.jpg',
    './../../../../../assets/images/carousel/Drum-pic-resized-1110x480.jpg',
    './../../../../../assets/images/carousel/ELC0hD4AwB0.jpg',
    './../../../../../assets/images/carousel/HMGNC_WTF_2-1110x480.jpg',
    './../../../../../assets/images/carousel/NCO1701_073017_TNU-170-Web-1110x480.jpg',
    './../../../../../assets/images/carousel/O2S_Xfdzgho.jpg',
    './../../../../../assets/images/carousel/Reading-with-hand-magnifier.jpg',
    './../../../../../assets/images/carousel/emotionheader.jpg',
    './../../../../../assets/images/carousel/r2HthctfubY.jpg'
  ];
  constructor() {}

  ngOnInit() {}
}
