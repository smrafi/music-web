import { MailService } from './../../services/mail.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from './../../services/storage.service';
import { StudentService } from './../../services/student.service';
import { InstructorService } from './../../services/instructor.service';
import { CourseService } from './../../services/course.service';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Howler, Howl } from 'howler';
import swal from 'sweetalert2';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  providers: [
    UserService,
    CourseService,
    InstructorService,
    StudentService,
    StorageService,
    MailService
  ]
})
export class HomepageComponent implements OnInit {
  studentData: any;
  courseData: any;
  instructorData: any;
  userData: any;
  value1: any;
  value2: any;
  value3: any;
  value4: any;
  access_group: any;
  url: any;

  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private instructorService: InstructorService,
    private courseService: CourseService,
    private userService: UserService,
    private studentService: StudentService,
    private storageService: StorageService,
    private mailService: MailService
  ) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      message: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.url = this.router.url;
    // console.log(this.router.url); //  /routename

    this.instructorService.getAll().subscribe((result: any) => {
      this.instructorData = result.data;
      this.value4 = this.instructorData.length;
    });
    this.courseService.getAll().subscribe((result: any) => {
      this.courseData = result.data;
      this.value3 = this.courseData.length;
    });
    this.userService.getAll().subscribe((result: any) => {
      this.userData = result.data;
      this.value2 = this.userData.length;
    });
    this.studentService.getAll().subscribe((result: any) => {
      this.studentData = result.data;
      this.value1 = this.studentData.length;
    });

    this.access_group = this.storageService.getCurrentAccessGroup();
    //console.log(this.access_group);
  }

  submitForm = ($event, value) => {
    console.log('mail send');
    $event.preventDefault();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
    }
    const formData: FormData = new FormData();
    formData.append('name', value.name);
    formData.append('email', value.email);
    formData.append('subject', value.subject);
    formData.append('message', value.message);

    this.mailService
      .create({
        name: value.name,
        email: value.email,
        subject: value.subject,
        message: value.message
      })
      .subscribe(() => {
        swal({
          title: 'Success!',
          text: 'Mail send',
          type: 'success',
          timer: 2000
        });
      });
  };
}
