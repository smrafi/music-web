import { TransectionComponent } from './transection.component';
import { Routes, RouterModule } from '@angular/router';

const childRoutes: Routes = [
  {
    path: '',
    component: TransectionComponent
  }
];

export const routing = RouterModule.forChild(childRoutes);
