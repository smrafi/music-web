import { PaymentService } from './../../../../services/payment.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transection-read',
  templateUrl: './transection.component.html',
  styleUrls: ['./transection.component.scss'],
  providers: [PaymentService]
})
export class TransectionComponent implements OnInit {
  data: any;
  constructor(private paymentService: PaymentService) {}
  ngOnInit() {
    this.paymentService.getAll().subscribe((result: any) => {
      this.data = result.data;
    });
  }
}
