import { FileUploadService } from './../../../../services/fileUpload.service';
import { CourseMaterialService } from './../../../../services/courseMaterial.service';
import { CourseService } from './../../../../services/course.service';
import { AuthService } from './../../../../services/auth.service';
import { AccessGroupService } from './../../../../services/access-group.service';
// import { InstructorService } from './../../../services/instructor.service';
// import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/';
import * as FileSaver from 'file-saver';
import swal from 'sweetalert2';
// import { UserService } from '../../../services';

@Component({
  selector: 'app-course-material',
  templateUrl: './course-material.component.html',
  styleUrls: ['./course-material.component.scss'],
  providers: [
    AuthService,
    UserService,
    CourseService,
    AccessGroupService,
    CourseMaterialService,
    FileUploadService
  ]
})
export class CourseMaterialComponent implements OnInit {
  currentUserInfo: any;
  currentUserWithAccess: any;
  data: any;
  _isSpinning: boolean = true;

  public items: any = [];
  result: any;
  selectedCourse: any;
  material: any;

  fileToUpload: File = null;

  constructor(
    private courseService: CourseService,
    private courseMaterialService: CourseMaterialService,
    private fileUploadService: FileUploadService
  ) {}

  ngOnInit() {
    this.getData();
  }

  async getData() {
    await this.courseService.getAll().subscribe((result: any) => {
      // //console.log(result);
      result.data.forEach(element => {
        this.items.push(element.course_name);
      });
    });
  }

  public refreshValue(value: any) {
    this.courseService
      .getByCourseName(this.selectedCourse)
      .subscribe((result: any) => {
        this.result = result.data[0].id;
        this.courseData(this.result);
      });
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log(this.fileToUpload);
  }

  ngOnDestroy(): void {}

  courseData(course_id: any) {
    this.courseMaterialService
      .getByCourseId(course_id)
      .subscribe((result: any) => {
        this.material = result.data;
        //console.log('bmbn', this.material);
      });
  }

  delete(id: any) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.courseMaterialService.delete(id).subscribe((result: any) => {
          this.courseData(this.result);
          // console.log(this.result);
        });
        swal('Deleted!', 'Course Material has been deleted.', 'success');
      }
    });
  }


  download(link, i) {
    this.fileUploadService
      .getFile(link)
      .subscribe((file: any) => this.downloadFile(file, i)), //console.log(data),
      error => console.log('Error downloading the file.'),
      () => console.info('OK');
  }

  downloadFile(data: any, i: any) {
    FileSaver.saveAs(data, this.material[i].material+'.pdf');
  }
}
