import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './password.routing';
import { SharedModule } from '../../admin-shared/shared.module';
import { PasswordComponent } from './password.component';
import { TooltipModule } from 'ng2-tooltip-directive';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing,
    FormsModule,
    TooltipModule,
    ReactiveFormsModule
  ],
  declarations: [PasswordComponent]
})
export class PasswordModule {}
