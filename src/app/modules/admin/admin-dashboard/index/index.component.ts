import { InstructorService } from './../../../../services/instructor.service';
import { CourseService } from './../../../../services/course.service';
import { MessageService } from './../../../../services/message.service';
import { CalendarEvent, DAYS_OF_WEEK } from 'angular-calendar';
import { PostService } from './../../../../services/post.service';
import { StudentService } from './../../../../services/student.service';
import { StorageService } from './../../../../services/storage.service';
import { StudentCourseService } from './../../../../services/studentcourse.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [
    CourseService,
    StorageService,
    StudentService,
    PostService,
    MessageService,
    InstructorService
  ]
})
export class IndexComponent implements OnInit {
  view: string = 'month';
  viewDate: Date = new Date();
  weekStartsOn: number = DAYS_OF_WEEK.SUNDAY;
  weekendDays: number[] = [DAYS_OF_WEEK.FRIDAY, DAYS_OF_WEEK.SATURDAY];
  events: CalendarEvent[] = [];

  clickedDate: Date;
  currentUserInfo: any;
  currentUserWithAccess: any;
  course_data: any;
  post_data: any;
  studentId: any;
  _isSpinning: boolean = true;
  user_id: any;
  receivedMessage: any;
  instructor_data: any;

  constructor(
    private storageService: StorageService,
    private postService: PostService,
    private messageService: MessageService,
    private courseService: CourseService,
    private instructorService: InstructorService
  ) {}

  async ngOnInit() {
    this.user_id = this.storageService.getCurrentUserId();

    this.postService.postByUser(this.user_id).subscribe((result: any) => {
      this.post_data = result.data[0];
      // console.log(this.post_data);
    });

    this.courseService.getAll().subscribe((result: any) => {
      this.course_data = result.data;
      // console.log(this.course_data);
    });

    this.instructorService.getAll().subscribe((result: any) => {
      this.instructor_data = result.data;
      // console.log(this.instructor_data);
    });

    this.messageService
      .messageToCurrentUser(this.user_id)
      .subscribe((result: any) => {
        this.receivedMessage = result.data;
        // console.log(this.receivedMessage);
      });
  }

  // delete(id: any) {
  //   this.courseService.delete(id).subscribe((result: any) => {
  //     this.getData();
  //   });
  // }

  ngOnDestroy(): void {}
}
