import { NotificationService } from './../../../../services/notification.service';
import swal from 'sweetalert2';
import {
  FormControl,
  Validators,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { MessageService } from './../../../../services/message.service';
import { StorageService } from './../../../../services/storage.service';
import { Component } from '@angular/core';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-messege-read',
  templateUrl: './messege.component.html',
  styleUrls: ['./messege.component.scss'],
  providers: [MessageService, UserService, StorageService, NotificationService]
})
export class MessegeComponent {
  currentUserId: any;
  data: any;
  items: any = [];
  result: any;
  selectedUser: any;
  material: any;
  message = new FormControl('', [Validators.required]);
  sentMessage: any;
  receivedMessage: any;
  currentUserName: any;
  validateForm: FormGroup;

  constructor(
    private userService: UserService,
    private storageServcie: StorageService,
    private messageService: MessageService,
    private notificationService: NotificationService,
    private fb: FormBuilder
  ) {
    this.validateForm = this.fb.group({
      receiver: ['', [Validators.required]],
      message: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.currentUserId = this.storageServcie.getCurrentUserId();
    this.currentUserName = this.storageServcie.getCurrentUser();
    this.getData();
    console.log(Boolean(this.selectedUser));
  }

  async getData() {
    // //console.log('this', this.currentUserName.user_name);
    // //console.log(this.currentUserId);
    await this.messageService
      .messageByCurrentUser(this.currentUserId)
      .subscribe((result: any) => {
        this.sentMessage = result.data;
        //console.log(result.data);
      });

    await this.messageService
      .messageToCurrentUser(this.currentUserId)
      .subscribe((result: any) => {
        this.receivedMessage = result.data;
        //console.log(result.data);
      });
    await this.userService.getAll().subscribe((result: any) => {
      result.data.forEach(element => {
        this.items.push({ label: element.user_name, value: element.id });
      });
    });
  }

  public refreshValue() {
    console.log(Boolean(this.selectedUser));
    //console.log(this.selectedUser);
  }

  submit($event, value) {
    console.log('hey');
    $event.preventDefault();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
    }
    this.messageService
      .create({
        sender_id: this.currentUserId,
        receiver_id: this.selectedUser,
        message: this.message.value
      })
      .subscribe((result: any) => {
        this.afterRefresh();
        //console.log('get data');
        this.getData();

        // notification
        swal({
          title: 'Success!',
          text: 'Message Send Succesfully',
          type: 'success',
          timer: 2000
        });
        this.notificationService
          .create({
            user_id: this.selectedUser,
            notification:
              this.currentUserName.user_name + '  send you a message',
            status: '1'
          })
          .subscribe(() => {});
      });
  }

  cancel() {
    document.getElementById('tab3').id = 'tab1';
  }

  afterRefresh() {
    this.message.setValue('');
  }

  ngOnDestroy(): void {}
}
