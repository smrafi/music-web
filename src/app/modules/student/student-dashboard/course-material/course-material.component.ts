import { PaymentService } from './../../../../services/payment.service';
import swal from 'sweetalert2';
import { FileUploadService } from './../../../../services/fileUpload.service';
import { StudentCourseService } from './../../../../services/studentcourse.service';
import { StudentService } from './../../../../services/student.service';
import { StorageService } from './../../../../services/storage.service';
import { CourseMaterialService } from './../../../../services/courseMaterial.service';
import { CourseService } from './../../../../services/course.service';
import { Component, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import { RaveOptions } from 'angular-rave';
// import swal

@Component({
  selector: 'app-course-material',
  templateUrl: './course-material.component.html',
  styleUrls: ['./course-material.component.scss'],
  providers: [
    CourseService,
    StorageService,
    StudentCourseService,
    CourseMaterialService,
    StudentService,
    FileUploadService,
    PaymentService
  ]
})
export class CourseMaterialComponent implements OnInit {
  lastPayment: any = 10;
  raveOptions: RaveOptions = {
    PBFPubKey: 'FLWPUBK_TEST-42a8dc72b857465645c7e9cf13603996-X',
    customer_email: '',
    customer_phone: '',
    amount: this.lastPayment,
    currency: 'USD',
    custom_title: '',
    txref: `${Math.random() * 1000000}`
  };
  currentUserInfo: any;
  currentUser: any;
  currentUserWithAccess: any;
  data: any;
  _isSpinning: boolean = true;
  studentId: any;
  paymentData: any;
  boughtMaterialList: any[] = [];
  public items: any = [];
  result: any;
  selectedCourse: any;
  material: any;
  constructor(
    private courseService: CourseService,
    private courseMaterialService: CourseMaterialService,
    private studentService: StudentService,
    private storageService: StorageService,
    private studentCourseService: StudentCourseService,
    private fileUploadService: FileUploadService,
    private paymentService: PaymentService
  ) {}

  async ngOnInit() {
    this.currentUserInfo = this.storageService.getCurrentUserId();

    this.checkBuy();

    this.currentUser = this.storageService.getCurrentUser();

    this.studentService
      .getStudentId(this.currentUserInfo)
      .subscribe((result: any) => {
        this.studentId = result.data[0].id;
        //console.log('this', this.studentId);
        this.studentCourseService
          .getByStudentId(this.studentId)
          .subscribe((result: any) => {
            //console.log(result.data);
            result.data.forEach(element => {
              this.items.push(element.course.course_name);
            });
          });
      });

    await this.courseService.getAll().subscribe((result: any) => {});
  }

  checkBuy() {
    this.paymentService
      .getByUserId(this.currentUserInfo)
      .subscribe((result: any) => {
        this.paymentData = result.data;
        this.paymentData.forEach(element => {
          this.boughtMaterialList.push(element.courseMaterial.id);
        });
        console.log(this.paymentData);
      });
  }

  public refreshValue(value: any) {
    this.courseService
      .getByCourseName(this.selectedCourse)
      .subscribe((result: any) => {
        this.result = result.data[0].id;
        this.courseData(this.result);
      });
  }

  ngOnDestroy(): void {}

  courseData(course_id: any) {
    this.courseMaterialService
      .getByCourseId(course_id)
      .subscribe((result: any) => {
        this.material = result.data;
        console.log('zxcZc', this.material);
        //console.log('bmbn', this.material);
      });
  }

  download(link, i) {
    this.fileUploadService
      .getFile(link)
      .subscribe((file: any) => this.downloadFile(file, i)), //console.log(data),
      error => console.log('Error downloading the file.'),
      () => console.info('OK');
  }

  downloadFile(data: any, i: any) {
    FileSaver.saveAs(data, this.material[i].material + '.pdf');
  }

  paymentFailure() {
    console.log('Payment Failed');
  }

  paymentSuccess(res, link, i, id) {
    swal('Success', 'Payment Completed', 'success');
    this.download(link, i);
    let payload: any = {
      user_id: this.currentUserInfo,
      courseMaterial_id: id,
      amount: this.raveOptions.amount,
      status: 2
    };
    this.paymentService.create(payload).subscribe((result: any) => {
      console.log(result);
      this.checkBuy();
    });
    console.log('Payment complete', res);
  }

  paymentInit(amount) {
    console.log('Payment about to begin');
  }

  onClick(amount) {
    this.raveOptions.amount = amount;
    this.raveOptions.customer_email = this.currentUser.email;
    this.raveOptions.customer_phone = this.currentUser.contact_no;
    console.log(this.raveOptions.amount);
  }

  checkPayment(id) {
    return this.boughtMaterialList.includes(id);
  }
}
