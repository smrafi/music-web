import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../../../services/auth.service';
import { UserService } from '../../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import * as bcrypt from 'bcryptjs';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
  providers: [AuthService, UserService]
})
export class PasswordComponent implements OnInit {
  type1 = 'password';
  type2 = 'password';
  show1 = false;
  show2 = false;
  eye1 = true;
  eye2 = true;
  hashedPassword: any;
  currentUser: any;
  data: any;
  current_user_id: any;
  passwordField = document.querySelector('#password');

  validateForm: FormGroup;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.validateForm = this.fb.group({
      password: ['', [Validators.required]],
      new_password: ['', [Validators.required]]
    });
  }

  async ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
    this.current_user_id = this.authService.getCurrentUserId();
    //console.log(this.currentUser.user_name);
  }

  async submit($event, value) {
    console.log('hey');
    $event.preventDefault();
    for (const key in this.validateForm.controls) {
      this.validateForm.controls[key].markAsDirty();
    }
    this.hashedPassword = await bcrypt.hashSync(value.new_password, 8);

    //console.log(this.password.value);

    //console.log(this.hashedPassword);
    this.authService
      .login(this.currentUser.user_name, value.password)
      .subscribe(result => {
        this.data = result;
        //console.log('password matched');

        this.userService
          .update(this.current_user_id, {
            password: this.hashedPassword
          })
          .subscribe(result => {
            swal({
              title: 'Success!',
              text: 'Password Changed Successfully.',
              type: 'success',
              timer: 2000
            });
            //console.log('password update succesful', result);
          });
        this.authService.logout();
        this.router.navigate(['/']);
      });
  }

  toggleShow1() {
    this.show1 = !this.show1;
    if (this.show1) {
      this.type1 = 'text';
      this.eye1 = false;
    } else {
      this.type1 = 'password';
      this.eye1 = true;
    }
  }
  toggleShow2() {
    this.show2 = !this.show2;
    if (this.show2) {
      this.type2 = 'text';
      this.eye2 = false;
    } else {
      this.type2 = 'password';
      this.eye2 = true;
    }
  }
}
