import { MessageService } from './../../../../services/message.service';
import { PostService } from './../../../../services/post.service';
import { StudentService } from './../../../../services/student.service';
import { StorageService } from './../../../../services/storage.service';
import { StudentCourseService } from './../../../../services/studentcourse.service';
import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../../../../shared/pages/charts/components/echarts/charts.service';
import { CalendarEvent } from 'angular-calendar';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [
    StudentCourseService,
    StorageService,
    StudentService,
    PostService,
    MessageService
  ]
})
export class IndexComponent implements OnInit {
  view: string = 'month';

  viewDate: Date = new Date();

  events: CalendarEvent[] = [];

  clickedDate: Date;
  currentUserInfo: any;
  currentUserWithAccess: any;
  course_data: any;
  post_data: any;
  studentId: any;
  _isSpinning: boolean = true;
  user_id: any;
  receivedMessage: any;

  constructor(
    private studentCourseService: StudentCourseService,
    private storageService: StorageService,
    private studentService: StudentService,
    private postService: PostService,
    private messageService: MessageService
  ) {}

  async ngOnInit() {
    this.user_id = this.storageService.getCurrentUserId();

    this.postService.postByUser(this.user_id).subscribe((result: any) => {
      this.post_data = result.data[0];
      // console.log(this.post_data);
    });

    this.studentService.getStudentId(this.user_id).subscribe((result: any) => {
      this.studentId = result.data[0].id;
      //console.log('this', this.studentId);
      this.studentCourseService
        .getByStudentId(this.studentId)
        .subscribe((result: any) => {
          this.course_data = result.data;
          // console.log(this.data);
        });
    });

    this.messageService
      .messageToCurrentUser(this.user_id)
      .subscribe((result: any) => {
        this.receivedMessage = result.data;
        // console.log(this.receivedMessage);
      });
  }

  // delete(id: any) {
  //   this.courseService.delete(id).subscribe((result: any) => {
  //     this.getData();
  //   });
  // }

  ngOnDestroy(): void {}
}
