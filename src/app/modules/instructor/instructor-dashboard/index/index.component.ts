import { CourseService } from './../../../../services/course.service';
import { InstructorService } from './../../../../services/instructor.service';
import { MessageService } from './../../../../services/message.service';
import { PostService } from './../../../../services/post.service';
import { StudentService } from './../../../../services/student.service';
import { StorageService } from './../../../../services/storage.service';
import { StudentCourseService } from './../../../../services/studentcourse.service';
import { Component, OnInit } from '@angular/core';
import { CalendarEvent } from 'angular-calendar';
import { ChartsService } from '../../../../shared/pages/charts/components/echarts/charts.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [
    InstructorService,
    StorageService,
    StudentService,
    PostService,
    MessageService,
    CourseService
  ]
})
export class IndexComponent implements OnInit {
  view: string = 'month';

  viewDate: Date = new Date();

  events: CalendarEvent[] = [];

  id: any;
  clickedDate: Date;
  currentUserInfo: any;
  currentUserWithAccess: any;
  course_data: any;
  post_data: any;
  studentId: any;
  _isSpinning: boolean = true;
  user_id: any;
  receivedMessage: any;

  constructor(
    private instructorService: InstructorService,
    private storageService: StorageService,
    private postService: PostService,
    private messageService: MessageService,
    private courseService: CourseService
  ) {}

  async ngOnInit() {
    this.user_id = this.storageService.getCurrentUserId();

    this.postService.postByUser(this.user_id).subscribe((result: any) => {
      this.post_data = result.data[0];
      // console.log(this.post_data);
    });

    this.instructorService
      .getByUserId(this.user_id)
      .subscribe((result: any) => {
        this.id = result.data[0].id;
        this.courseService
          .getByInstructorId(this.id)
          .subscribe((result: any) => {
            this.course_data = result.data;
          });
      });

    this.messageService
      .messageToCurrentUser(this.user_id)
      .subscribe((result: any) => {
        this.receivedMessage = result.data;
        // console.log(this.receivedMessage);
      });
  }

  // delete(id: any) {
  //   this.courseService.delete(id).subscribe((result: any) => {
  //     this.getData();
  //   });
  // }

  ngOnDestroy(): void {}
}
