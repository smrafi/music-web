export let MENU_ITEM = [
  {
    path: 'index',
    title: 'Dashboard',
    icon: 'dashboard'
  },

  {
    path: 'course',
    title: 'Course',
    icon: 'file-text-o',
    children: [
      {
        path: 'all-course',
        title: 'My Course'
      }
    ]
  },
  // {
  //   path: 'student',
  //   title: 'Student',
  //   icon: 'paint-brush',
  //   children: [
  //     {
  //       path: 'all-student',
  //       title: 'All Student'
  //     }
  //   ]
  // },
  {
    path: 'course-material',
    title: 'Course Material',
    icon: 'clone'
  },
  {
    path: 'post',
    title: 'Post',
    icon: 'pencil-square-o',
    children: [
      {
        path: 'my-post',
        title: 'My Post'
      },
      {
        path: 'create-post',
        title: 'Create Post'
      }
    ]
  },
  {
    path: 'result',
    title: 'Result',
    icon: 'line-chart',
    children: [
      {
        path: 'result-create',
        title: 'Manage Result'
      }
      // {
      //   path: 'result-create',
      //   title: 'Upload Result'
      // }
    ]
  },
  {
    path: 'messege',
    title: 'Messege',
    icon: 'envelope'
  }
];
