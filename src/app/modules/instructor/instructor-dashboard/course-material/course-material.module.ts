import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng2-select';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './course-material.routing';
import { SharedModule } from '../../instructor-shared/shared.module';
import { CourseMaterialComponent } from './course-material.component';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    routing,
    SelectModule,
    FormsModule,
    FileUploadModule,
    ReactiveFormsModule
  ],
  declarations: [CourseMaterialComponent]
})
export class CourseMaterialModule {}
