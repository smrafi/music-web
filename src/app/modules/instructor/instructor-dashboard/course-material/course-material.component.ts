import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileUploadService } from './../../../../services/fileUpload.service';
import { FileUploader } from 'ng2-file-upload';
import { InstructorService } from './../../../../services/instructor.service';
import { StorageService } from './../../../../services/storage.service';
import { CourseMaterialService } from './../../../../services/courseMaterial.service';
import { CourseService } from './../../../../services/course.service';
import { AuthService } from './../../../../services/auth.service';
import { AccessGroupService } from './../../../../services/access-group.service';
import * as FileSaver from 'file-saver';

// import { InstructorService } from './../../../services/instructor.service';
// import { AuthService } from './../../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../services/';
import swal from 'sweetalert2';
import 'rxjs/Rx';
// import { UserService } from '../../../services';

// const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
const URL = 'http://localhost:3000/api/v1/fileuploads/uploadfile';

@Component({
  selector: 'app-course-material',
  templateUrl: './course-material.component.html',
  styleUrls: ['./course-material.component.scss'],
  providers: [
    AuthService,
    UserService,
    CourseService,
    AccessGroupService,
    CourseMaterialService,
    InstructorService,
    FileUploadService
  ]
})
export class CourseMaterialComponent implements OnInit {
  file: any;
  currentUserInfo: any;
  id: any;
  currentUserWithAccess: any;
  data: any;
  _isSpinning: boolean = true;
  currentUserId: any;
  public items: any = [];
  result: any;
  selectedCourse: any;
  material: any;
  files: any[];
  fileName: any;
  validateForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private courseService: CourseService,
    private courseMaterialService: CourseMaterialService,
    private storageService: StorageService,
    private instructorService: InstructorService,
    private fileUploadService: FileUploadService
  ) {
    this.validateForm = this.fb.group({
      material: ['', []],
      price: ['', []],
      link: ['', []]
    });
  }

  async ngOnInit() {
    this.currentUserId = this.storageService.getCurrentUserId();

    await this.instructorService
      .getByUserId(this.currentUserId)
      .subscribe((result: any) => {
        this.id = result.data[0].id;
        this.getData();
      });
  }

  getData() {
    //console.log(this.id);
    this.courseService.getByInstructorId(this.id).subscribe((result: any) => {
      result.data.forEach(element => {
        this.items.push(element.course_name);
      });
    });
  }
  public refreshValue(value: any) {
    this.courseService
      .getByCourseName(this.selectedCourse)
      .subscribe((result: any) => {
        this.result = result.data[0].id;
        this.courseData(this.result);
      });
  }

  ngOnDestroy(): void {}

  courseData(course_id: any) {
    this.courseMaterialService
      .getByCourseId(course_id)
      .subscribe((result: any) => {
        this.material = result.data;

        // console.log('bmbn', this.material);
      });
  }

  delete(id: any) {
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) {
        this.courseMaterialService.delete(id).subscribe((result: any) => {
          this.courseData(this.result);
          // console.log(this.result);
        });
        swal('Deleted!', 'Course Material has been deleted.', 'success');
      }
    });
  }

  public uploader: FileUploader = new FileUploader({ url: URL });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  fileChange(event) {
    // if (event.event && event.event.percent === 100) {
    this.files = event.target.files;
    const formData = new FormData();
    formData.append('file', this.files[0], this.files[0].name);
    this.fileUploadService.uploadFile(formData).subscribe((result: any) => {
      // console.log(result);
      this.fileName = result.file.filename;
    });
    // }
  }

  submitForm = (event, value) => {
    this.file = value.material;
    console.log(this.result);
    console.log(value);
    let payload = {
      material: value.material,
      price: value.price,
      link: this.fileName,
      course_id: this.result
    };
    this.courseMaterialService.create(payload).subscribe((result: any) => {
      console.log(result);
    });
  };

  download(link, i) {
    this.fileUploadService
      .getFile(link)
      .subscribe((file: any) => this.downloadFile(file, i)), //console.log(data),
      error => console.log('Error downloading the file.'),
      () => console.info('OK');
  }

  downloadFile(data: any, i: any) {
    FileSaver.saveAs(data, this.material[i].material + '.pdf');
  }
}
